    /* ************************************************************************** */
    /*                                                                            */
    /*                 RAYTRACER                              :::      ::::::::   */
    /*                                                      :+:      :+:    :+:   */
    /*                                                    +:+ +:+         +:+     */
    /*    By: caupetit <caupetit@student.42.fr>          +#+  +:+       #+        */
    /*    By: tmielcza <tmielcza@student.42.fr>       +#+#+#+#+#+   +#+           */
    /*    By: cmartin   <cmartin@student.42.fr>            #+#    #+#             */
    /*    By: shedelin <shedelin@student.42.fr>           ###   ########.fr       */
    /*                                                                            */
    /* ************************************************************************** */

# Welcome to Raytracer project #

****

This is a group project.

Only works on mac OSX. A simple implementation c standar functions is used. See [libft/](https://bitbucket.org/caupetit/raytracer-parsing-3d-engine/src/806b1442de30ab9a0a1b779e7df5f11a8f1b8ccb/libft/?at=master)

This program display 3D files using raytracing method in a simple graphic interface.

Needed MinilibiX graphic library: https://cdn.42.fr/elearning/INFOG-1-001/mlx-20140106.tgz

 - AUTOMATONS are used to parse 3D files. See [scene.h](https://bitbucket.org/caupetit/raytracer-parsing-3d-engine/src/2f339a89cf3256158a1b1ccd8c4df0083360de65/scene.h?at=master) to know how to create such a file.

 - Exemple of 3D file [here](https://bitbucket.org/caupetit/raytracer-parsing-3d-engine/src/9a441f0b5d758603338211786b4f5236d609e44b/map.sc?at=master).

****

Use:

- make

- ./RT

****

- Basic scene showing primitives and features.

![Screen Shot Raytracer - Exemple scene.png](https://bitbucket.org/repo/M7gAaA/images/2374751987-Screen%20Shot%20Raytracer%20-%20Exemple%20scene.png)

- 10 Materials we configured. they can be set to every primitives.

![Screen Shot Raytracer - Materials.png](https://bitbucket.org/repo/M7gAaA/images/3333959383-Screen%20Shot%20Raytracer%20-%20Materials.png)

- This sccene shows differents materials set to primitives. We can see Fresnel in mulit level reflexion (blur), colored multi spots etc.

![Screen Shot Raytracer - Big scene.png](https://bitbucket.org/repo/M7gAaA/images/3209920556-Screen%20Shot%20Raytracer%20-%20Big%20scene.png)

- Exemple of basic scene showing shadows, shining and reflexion.

![Screen Shot Raytracer - shadows.png](https://bitbucket.org/repo/M7gAaA/images/2188153625-Screen%20Shot%20Raytracer%20-%20shadows.png)

- pausing during calculation.

![Screen Shot Raytracer - pause.png](https://bitbucket.org/repo/M7gAaA/images/137591889-Screen%20Shot%20Raytracer%20-%20pause.png)

- some fails we got during work on project:

![Screen Shot Raytracer - fail 1.png](https://bitbucket.org/repo/M7gAaA/images/3967851274-Screen%20Shot%20Raytracer%20-%20fail%201.png)